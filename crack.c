#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "md5.h"
#include "bintree.h"

const int PASS_LEN=50;        // Maximum any password can be
const int HASH_LEN=33;        // Length of MD5 hash strings

// TODO
// Read in the hash file and return the array of strings.
// Use the technique we showed in class to expand the
// array as you go.
char **read_hashes(char *filename)
{
     FILE * f = fopen(filename,"r");   // Open file to read hashes from
         
    int size = 50;                    // initial number of entries to the array
    
    char ** hasharray = (char **)malloc(size * sizeof(char *)); // Array initialized 
    
    for( int k = 0; k < size; k++){   
        hasharray[k] =(char *)malloc(33);               // memory allocated for individual strings
    }
    
    int i = 0;
    int j = size;   
        
    while( fscanf(f,"%s\n", hasharray[i]) != EOF){  //Reads in the file to the array
    
        if( i == (size - 1)){  //if the array is too small, make the size larger and reallocate memory
            size += size;
            char ** newarray = (char **)realloc(hasharray,size * sizeof(char *));
            hasharray = newarray;
            while( j < size){
                hasharray[j] = (char *)malloc(33);  //also, allocate memory for the new strings           
                j++;
            }
       }      
       i++; 
    }    
    fclose(f);          //close the file
    return hasharray;   //return the array
}


// TODO
// Read in the dictionary file and return the tree.
// Each node should contain both the hash and the
// plaintext word.
node *read_dict(char *filename)
{
    
    node *tree =(struct node*)malloc(sizeof(struct node));              // instantiate the binary tree
       
    FILE * f = fopen(filename,"r"); // open file
    
    char * word = (char *)malloc(50 * sizeof(char));
    char * wordhash = (char *)malloc(33 * sizeof(char));         // create all of the necessary variables
    node * leaf = NULL;
   
    while( fscanf(f,"%s\n",word) != EOF)
    {                    
        
         
        wordhash = md5(word,strlen(word));  // value for "hash" in each leaf       
        
        insert(wordhash, &tree);            // creating each leaf with the wordhash variable

        leaf = search(wordhash,tree);     // finding the location of each leaf
        int i = 0;
        while( word[i]){
            *(leaf->word +i) = word[i];
            i++;
        }
    }   
 
    free(word);   
    
    return tree;
}


int main(int argc, char *argv[])
{
    
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }   
    
    char * hashfile = argv[1];
    char * dictfile = argv[2];
    
    // TODO: Read the hash file into an array of strings
    char **hashes = read_hashes(hashfile);

    // TODO: Read the dictionary file into a binary tree
    node *dict = read_dict(dictfile);


    // TODO
    // For each hash, search for it in the binary tree.
    // If you find it, get the corresponding plaintext dictionary
    // entry. Print both the hash and word out.
    // Need only one loop. (Yay!)
    
    int position = 0; //Keeps track of how far into the array we are
    node *leaf = NULL;
    
    
    while( hashes[position] != NULL){
        leaf = search(hashes[position],dict);
        if(leaf == NULL){
            //free(hashes[position]);
            position++;
        }
        else{            
            printf("%s %s\n",hashes[position],leaf->word);
            free(hashes[position]);
            position++;
        }       
    }       

    return 0;
}
