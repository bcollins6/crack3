#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "bintree.h"

void insert(char * key, node **leaf)
{
    if (*leaf == NULL)
    {
        *leaf = (struct node *)malloc(sizeof(struct node));
        (*leaf)->hash = (char *)malloc(33);     //create space for the hash
        (*leaf)->word = (char *)malloc(50);     //create space for the word
        (*leaf)->hash = key;
        /* initialize the children to null */
        (*leaf)->left = NULL;    
        (*leaf)->right = NULL;  
    }
    else if (strcmp(key,(*leaf)->hash) < 0)
        insert (key, &((*leaf)->left) );
    else if (strcmp(key,(*leaf)->hash) > 0)
        insert (key, &((*leaf)->right) );
}

void print(node *leaf)
{
	if (leaf == NULL) return;
	if (leaf->left != NULL) print(leaf->left);
	printf("%s %s\n",(*leaf).word,(*leaf).hash);
	if (leaf->right != NULL) print(leaf->right);
}

// TODO: Modify so the key is the hash to search for
node *search(char * key, node *leaf)
{
    
  if (leaf != NULL)
  {
      if (strcmp(key,leaf->hash) == 0)
         return leaf;
      else if (strcmp(key,leaf->hash) < 0)
         return search(key, leaf->left);
      else
         return search(key, leaf->right);
  }
  else return NULL;
}

/*
int main()
{
    node *tree = NULL;
    
    insert("abc",&tree);
    struct node * leaf = search("abc",tree);
    leaf->word = (char*)malloc(20);
    leaf->word = "test";
    insert("dfg",&tree);
    leaf = search("df",tree);
    if( leaf != NULL){
        leaf->word = (char*)malloc(20);
        leaf->word = "working?";
    }
    insert("aab",&tree);
    insert("zxy",&tree);
    
    print(tree);
}
*/

